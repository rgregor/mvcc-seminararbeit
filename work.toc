\contentsline {section}{\numberline {1}Introduction}{1}
\contentsline {section}{\numberline {2}The origins of Multiversion Concurrency Control}{1}
\contentsline {section}{\numberline {3}How multiple data versions boost parallelism}{2}
\contentsline {section}{\numberline {4}Multiversion Concurrency Control Algorithms}{2}
\contentsline {subsection}{\numberline {4.1}Multiversion Timestamping Algorithm}{3}
\contentsline {section}{\numberline {5}Snapshot Isolation and Anomalies}{3}
\contentsline {paragraph}{Origin and motivation.}{3}
\contentsline {paragraph}{Definition.}{4}
\contentsline {paragraph}{Anomalies.}{4}
\contentsline {section}{\numberline {6}Performance}{5}
\contentsline {subsection}{\numberline {6.1}The Performance of Multiversion Algorithms}{5}
\contentsline {subsection}{\numberline {6.2}Storage and Querying}{6}
\contentsline {section}{\numberline {7}Advanced applications of MVCC}{6}
\contentsline {paragraph}{Multilevel secure database management systems.}{6}
\contentsline {paragraph}{Mobile database systems.}{6}
\contentsline {section}{\numberline {8}Conclusion}{7}
\contentsline {section}{References}{8}
