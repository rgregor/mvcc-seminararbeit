\documentclass[a4paper, 14pt]{scrartcl}

\usepackage [T1] {fontenc}
\usepackage {lmodern}
\usepackage [utf8] {inputenc}
\usepackage{listings}

\bibliographystyle{plain}

\title{An overview of Multiversion Concurrency Control}
%\date{\today} 

\begin{document}

\maketitle

\begin{center}by Gregor Riegler, 0703762\end{center}
\leavevmode\newline\newline\newline\newline\newline\newline\newline\newline
\section*{Abstract.} It is this thesis' goal to give a broad overview of Multiversion Concurrency Control, ranging from main ideas of MVCC, its algorithmic representations, advantages, drawbacks, performance studies to advanced applications of MVCC. 
\newline
As a result, 
\newpage

\tableofcontents
\thispagestyle{empty}
\newpage

\clearpage
\pagenumbering{arabic} 

\section{Introduction}
This thesis aims to give the reader an overview of Multiversion Concurrency Control (MVCC), including its origins, algorithmic definitions and implementations and advanced applications.
This will be accomplished by traversing the MVCC literature landscape from its beginnings until recent novelties.\newline
In the sections \ref{origins} and \ref{parallelism} the main idea of MVCC are introduced. An algorithmic formulation is then given in section \ref{algorithms}. After that, section \ref{anomalies} introduces MVCC's real-world companion \emph{Snapshot Isolation} and recent attempts to make that one serializable. Section \ref{performance} focuses on the performance of MVCC and describes how MVCC copes with various transaction workload types. Finally, section \ref{advanced} introduces applications of MVCC in non-conventional types of database systems with a focus on current research.

\section{The origins of Multiversion Concurrency Control}\label{origins}

Multiversion concurrency control has become popular through the work of Bernstein's and Goodman's \emph{Concurrency Control in Distributed Database Systems} \cite{Bernstein:1981} in 1981.
In that article Bernstein and Goodman analyze the problems and challenges which have to be dealt with in a distributed database system in comparison to a centralized transaction-processing model.
Furthermore, they provide a decomposition of the concurrency problem into the subproblems of two synchronization techniques, one dealing with Read-Write conflicts, the other with Write-Write conflicts. As until 1981 virtually all concurrency control methods had used a single synchronization method, such as two-phase locking (2PL) or timestamp-ordered (T/O) scheduling, that decomposition and integration made it possible to use a mix of synchronization methods for one intregrated concurrency control method. In order to combine various 2PL and T/O techniques, locks and timestamp had to be somehow \emph{unified} -- ``to interface 2PL and T/O we use locked points to induce timestamps'' (\cite{Bernstein:1981} p.27). One of the featured T/O synchronization methods is \emph{Multiversion T/O} which turns out to be an improvement to a bare T/O scheduler in the realm of the Read-Write synchronization subproblem. It uses a multiversion timestamping algorithm which originates from \cite{Reed:1978} and is introduced 
in section \ref{alg:timestamp}. 

\section{How multiple data versions boost parallelism}\label{parallelism}
Primarily, it is any concurrency control method's task to ensure the correctness of the interleaving of concurrent transactions with respect to some isolation level, secondarily, that interleaving should be done in a way which serves best parallelism and throughput. It is MVCC's approach to gain that performance boost by storing and supplying multiple versions of data, e.g. MVCC is built on the concept of \emph{multiversion data items} which were first introduced in \cite{Reed:1978}. The main idea of MVCC is nicely outlined in \cite{Papa:1984}, p. 90: \emph{``When a transaction issues a write step on some entity X, we might choose not to overwrite the old value of x by the new one, but to keep both versions. If subsequently another transaction reads X, we have the option of supplying to it either version, whichever serves serializability best, that being our ultimate goal.''}
\newline
\newline
In contrast to using locking synchronization methods, using MVCC a read action on data item x does not need to acquire a lock. In case a concurrent transaction updates the value of x the reader's transaction can just make use of a prior version of x. As a consequence, readers never block writers and writers never block readers; furthermore, readers never block readers which makes MVCC particularly performant for concurrent \emph{read} operations. In addition, it is the specific MVCC algorithm's reponsibility to abort transactions in case of conflicts and to restart them (see section \ref{algorithms}).
\newline
\newline
So, using multiple data versions it is possible to derive schedules, interleaved executions of transactions, achieving a level of parallelism higher than possible with one version per data only. In \cite{Papa:1984} theoretical work is done strengthening that proposition. Interestingly, it is shown that there is no strict limit on the number of versions to retain as to their usefulness in making histories multiversion-serializable (\cite{Papa:1984} p.91): \emph{``By keeping up to k + 1 versions of each entity one can “serialize” more schedules than with k only''}. Intuitively, this means that a MVCC database using \emph{x+1} version is more likely to achieve better parallelism than an implementation with only \emph{x} versions.  

\section{Multiversion Concurrency Control Algorithms}\label{algorithms}

There have been published various algorithms implementing MVCC, for instance in \cite{Bernstein:1982} and \cite{Bernstein:1983}. Multiversion timestamping, multiversion locking and mixed timestamping-locking implementations count among them; in order to prove their correctness it's not only necessary to consider the order of \emph{read} and \emph{write} operations but also the \emph{translation}, they way \emph{reads} of data items are translated to \emph{reads} of versions. In \cite{Bernstein:1983} Bernstein and Goodman present a full-fledged proof of correctness; they introduce the notion of \emph{one-copy serializability} (1-SR) of an execution and derive the above algorithms' correctness by showing that all their executions are 1-SR.
\newline
Generally, it is the multiversion timestamping algorithm, and variations thereof using locks for write access respectively, which have become the most accepted MVCC algorithm. It has proven to be a viable extension of databases already using timestamped transactions and optimistic concurrency control. Subsequently, the algorithm is introduced based on its description in \cite{Bernstein:1983}. 

\subsection{Multiversion Timestamping Algorithm}\label{alg:timestamp}
Each transaction has to be assigned a unique \emph{timestamp} when its execution starts. Obviously, that timestamp indicates the time of the start of the transaction in so far as it must be comparable to the timestamps of other transaction. (It follows that in a variation of the algorithm timestamps could be replaced by increasing transaction ids without changing the algorithm at all!)
\newline
Each \emph{write} and \emph{read} operation is dealt with together with the timestamp of the transaction that issued it. In addition, each data item version carries the timestamp (or id) of the transaction which wrote it. 

\begin{enumerate}
\item Transaction T with timestamp b reading some data item is translated to reading the version x of the respective data item such that x is the largest timestamp of an available version smaller b.
\item Transaction T with timestamp b wants to write a new version of some data item. If a concurrent transaction V with timestamp c has been granted reading version a of the data item such that a<b<c, then the \emph{write} is rejected and the transaction is restarted. Otherwise the \emph{write} is allowed and a new version b of the data item is stored.
\end{enumerate}

\section{Snapshot Isolation and Anomalies}\label{anomalies}
Nowadays, MVCC is used heavily in many database system to achieve so-called \emph{Snapshot Isolation}. This may be due to the fact that it provides higher throughput than strict two-phase locking, above all in read-heavy workloads (\cite{Cahill:2008}, p.730). Its current popularity is reason enough to have a look on its origin, motivation and definition in this section.
\paragraph{Origin and motivation.} \emph{``A critique of ANSI SQL isolation levels''} \cite{Berenson:1995} is the publication which introduces snapshot isolation in 1995. Essentially, that publication critizises the ANSI SQL-92 definitions of isolation levels. The isolation levels' defining criterion is what unwanted \emph{phenomena} as \emph{Dirty Read}, \emph{Non-repeatable Read} and \emph{Phantom} they prohibit; \cite{Berenson:1995} claims that those \emph{phenomena} are ambigious, though (\cite{Berenson:1995} p.2). It's for that reason that it presents better formal definitions of the \emph{phenomena} and characterizes the isolation levels in terms of the (at that time) predominant lock-based isolation. Finally, that intense preoccupation with the existing isolation levels and their definitions lead the authors of \cite{Berenson:1995} to the conclusion that yet another isolation level is worth being considered -- it lies between \emph{read committed} and \emph{repeatable read} and they call it \emph{
Snapshot Isolation}.
\paragraph{Definition.} In snapshot isolation, transactions are provided with a consistent snapshot of the world as of their start time of execution (\emph{= Start-Timestamp}). A transaction reading a data item x is never blocked as long as some snapshot from its Start-Timestamp can be provided. A write operation's effects are only visible to the snapshot of the transaction which issued it and are invisible to concurrent transactions active before the transaction Start-Timestamp. When a transaction T commits the changes become visible to all transactions with a Start-Timestamp greater than T's Commit-Timestamp. 
\newline
It is obvious that the requirements of snapshot isolation go nicely with the sample multiversion algorithm presented in \ref{alg:timestamp}. A MVCC database system thus allows a natural implementation of the snapshot isolation level. An amendment is necessary to add in step 2, though. Upon Commit-Time, a transaction T which wants to write some new data item version must make sure that no other transaction with a Commit-Timestamp greater than T's Start-Timestamp has rewritten the same data item. In case of such a conflict T has to abort to prevent the \emph{Lost Update} phenomenon.
\paragraph{Anomalies.}
The definition of snapshot isolation still allows transactions which are not \emph{serializable} as snapshot isolation is susceptible to so-called \emph{anomalies}, e.g. constraint violations over multiple data items or an anomaly called \emph{write skew} (see \cite{Cahill:2008}, p. 729). Recent research has thus sought to detect and prevent those anomaly instances in order to provide serializable isolation.
\newline
\newline
Many attempts of getting rid of the anomalies focus on the application programs and the question if they are guaranteed to produce serializable executions when they run on a system using snapshot isolation. Among these publications aiming at the application code to make serializability guarantees count \cite{Fekete:2005} and \cite{Fekete:2005b}. One of the proposed measures consists in adding artificial \emph{writes} on data items which should have only be \emph{read} in the first place; as a result, various instances of \emph{write skew} can be prevented. According to 
\cite{fekete:2008} it is thus possible to efficiently ensure serializability under snapshot isolation.  
\newline
A different approach is conducted by \cite{Cahill:2008} and \cite{Tilgner:2011}; they aim to ensure the serializability property through the snapshot isolation algorithm itself. \cite{Cahill:2008} provides a sample implementation and finds that the conservative serializable snapshot implementation still performs significantly better than two-phase locking in a variety of cases whereas bare snapshot isolation is still better because of lower transaction abort rates.

% Multi-version Concurrency via Timestamp Range Conflict Management - provides mv irgendwo zu allen isolation levels
 


\section{Performance}\label{performance}

\subsection{The Performance of Multiversion Algorithms}
Concurrency control algorithms are very difficult to compare with regard to performance -- variable parameters as the size of transactions, the distribution of \emph{read} and \emph{write} operations, the resulting conflicts, I/O and storage costs make the survey of handling concurrent transactions pretty complex and indeterministic and therefore very hard to formalize. Therefore, simulations rather than formal comparisons have been proposed to analyze MVCC performance.
\newline
\newline
An early work of \cite{Carey:1986} using simulation analyzes the performance of multiversion algorithms with each other as well as with their single-version counterparts. That analysis considers the level of concurrency as well as CPU, I/O and storage costs.
\newline
One simulated scenario examines the algoriths' behaviors coping with small update-transactions and long read-only transactions, an experiment for which MVCC was designed to prevail. In fact, the MVCC algorithms proved to be better than any tested single-version strategy -- above all, the predominant conflicts of \emph{read} and \emph{update} transactions occurring in lock-based strategies could be eliminated as the MVCC algorithms can provide older data versions to the read-only transactions.
\newline
A different scenario highlights under which Multiversion Timestamp-Ordered Concurrency Control performs badly: under the dominance of update-transactions. Under a low number of concurrent update-transactions it still performs similar to multiversion two-phase locking and singleversion two-phase locking, however, the more the number of update-transactions increases the more lock-based approaches shine. Essentially, the time costs of restart transactions dominate the time costs of being blocked by locks in that scenario.
\newline\newline
The recent work of \cite{disanzo:2008} provides the first analytical model capturing the performance of MVCC algorithms.
That model abstracts away from the costs of querying and storing as well as from the chosen strategy of storing multiple versions or reconstructing them (see section \ref{storage}). Assuming that the analytical model is precise enough, further research could benefit from its hardware-independent, inexpensive performance analysis.    

\subsection{Storage and Querying}\label{storage}
In the case of MVCC it's obvious that storing and retrieving multiple data versions goes with a certain overhead in space consumption in comparison to one-version synchronization strategies. Consequently, the process of reading and writing data versions is more complex and potentially more time-consuming; for pratical implementations, it's thus essential to find appropriate data structures to mitigate those negative effects on the overall performance. That quest is subject to ongoing research and for instance dealt with in \cite{Lomet:1989}, \cite{Becker:1996}. Those works present multiversion B+-trees as appropriate index structures for MVCC; the recent work of \cite{Haapasalo:2009} extends the capabilities of former approaches and proposes a concurrent multiversion B+-tree for efficiently storing and querying multiversion data.
\newline
An alternative approach to storing multiple versions is implemented for example in Oracle's database -- upon querying older versions are reconstructed from the current version, see \cite{Oracle:2005}.

\section{Advanced applications of MVCC}\label{advanced}
In this final section two types of database systems are presented for which the multiversion concurrency approach has great importance. It will be explained why MVCC is a wise choice to solve the problems of those contexts.
\paragraph{Multilevel secure database management systems.}
In contrast to conventional database systems multilevel secure database management systems must manage the access of security-classified data to users with certain clearances. These requirements have a strong impact on the concurrency control method to use -- it's a fact that various standard methods as two-phase locking or optimistic concurrency methods are not secure in this context. They are susceptible to so-called \emph{covert channel} attacking methods as described in \cite{kaur:2009}. Basically, it's the contention for locks and possible starvation of transactions, especially high-level transaction, which makes standard methods vulnerable in the multilevel secure context. As MVCC's reading operations do not block writers one of those problem sources can be eliminated nicely using MVCC. A proposal of a starvation-free MVCC protocol for that purpose can be found in \cite{Kim:1998} and in 2009 an improvement of the former was published by \cite{kaur:2009}.
%forgetting versions - mvcc overview arbeit mit garbage collection erwähnen
\paragraph{Mobile database systems.}
In recent years the use of mobile clients has skyrocketed. As a result, the importance of Mobile Database Systems has emerged; among the challenges of the mobile context count data processing delays, temporal consistency, stale data versions, timing constraints and mobile transactions. Here the multiversion approach offers a natural handling of stale data -- as multiple data versions are stored anyway \emph{read} operations can be provided with old versions up to a certain maximum degree of staleness. Just as important in the mobile context is the handling of out-of-date transactions trying to write already outdated data -- a \emph{Semantic based transaction processing model} permitting this for commutative operations is given in \cite{Yakovlev:2005}. \cite{Madria:2007} introduces a deadlock-free concurrency-control mechanism based on a multiversion two-phase locking scheme.
\newline
\newline
All in all, MVCC has proven to be a serious contender to all other concurrency control mechanisms especially in database contexts with non-mainstream requirements as in multilevel secure, mobile and real-time database systems. Another work demonstrating MVCC's flexibility in this field is \cite{park:2002} which introduces a multiversion locking protocol designed for secure real-time database systems. 

\section{Conclusion}\label{sec:conclusion}
In this work an overview of Multiversion Concurrency control was given. It has been shown how multiple versions can boost parallelism and how that idea can be compiled into an algorithmic procedure. In addition, the notion of isolation levels has been touched upon in the description of the birth of snapshot isolation of which MVCC constitutes the most natural implementation; in that section a small taste of how MVCC can be tweaked to achieve serializable snapshot isolation has been provided. Similar flexibility of MVCC has then been discovered in the realm of multilevel secure, mobile and real-time database systems where unconventional requirements complicate concurrency control. It is particularly in that fascinating field of mobile and real-time database systems, variations thereof respectively, where potential for future innovations of MVCC transaction processing models can be assumed and future work might yield fruit.   

\newpage
\renewcommand\refname{References}
\addcontentsline{toc}{section}{References}
\bibliography{literature}




\end{document}
